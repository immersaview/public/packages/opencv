stages:
  - build
  - package
  - deploy

variables:
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: recursive

#-----------------------------------------------------------
#                      TEMPLATES
#-----------------------------------------------------------

.build_generic_template: &generic_build_definition
  stage: build
  artifacts:
    expire_in: 4 hours
  script:
    - bash "./build.sh" "$OS" "$ARCH" "$CONFIG" "$TOOL"

.build_win_template: &win_build_definition
  <<: *generic_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.1-windows2019
  tags:
    - imv-windows-docker
  artifacts:
    paths:
      - "$OS/$TOOL/$ARCH/opencv2/*.h"
      - "$OS/$TOOL/$ARCH/opencv2/*.hpp"
      - "$OS/$TOOL/$ARCH/source/lib/$CONFIG/*.lib"
      - "$OS/$TOOL/$ARCH/source/3rdparty/lib/$CONFIG/*.lib"

.build_lnx_template: &lnx_build_definition
  <<: *generic_build_definition

  artifacts:
    paths:
      - "$OS/$TOOL/$ARCH/$CONFIG/source/lib/*.a"
      - "$OS/$TOOL/$ARCH/$CONFIG/source/3rdparty/lib/*.a"

#-----------------------------------------------------------
#                      WINDOWS BUILDS
#-----------------------------------------------------------

build_win_vs2019_x64_rel:
  <<: *win_build_definition

  variables:
    OS: "windows"
    ARCH: "x64"
    CONFIG: "Release"
    TOOL: "vc142"

build_win_vs2019_x64_dbg:
  <<: *win_build_definition

  variables:
    OS: "windows"
    ARCH: "x64"
    CONFIG: "Debug"
    TOOL: "vc142"

build_win_vs2019_x86_rel:
  <<: *win_build_definition

  variables:
    OS: "windows"
    ARCH: "x86"
    CONFIG: "Release"
    TOOL: "vc142"

build_win_vs2019_x86_dbg:
  <<: *win_build_definition

  variables:
    OS: "windows"
    ARCH: "x86"
    CONFIG: "Debug"
    TOOL: "vc142"

#-----------------------------------------------------------
#                      UBUNTU BUILDS
#-----------------------------------------------------------

build_deb_x64_rel:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  variables:
    OS: "ubuntu"
    ARCH: "x64"
    CONFIG: "release"
    TOOL: "gcc"

build_deb_x64_dbg:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  variables:
    OS: "ubuntu"
    ARCH: "x64"
    CONFIG: "debug"
    TOOL: "gcc"

#-----------------------------------------------------------
#                      CENTOS BUILDS
#-----------------------------------------------------------

build_rpm_x64_rel:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7
  variables:
    OS: "centos"
    ARCH: "x64"
    CONFIG: "release"
    TOOL: "gcc"

build_rpm_x64_dbg:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7
  variables:
    OS: "centos"
    ARCH: "x64"
    CONFIG: "debug"
    TOOL: "gcc"

#-----------------------------------------------------------
#                      PACKAGE
#-----------------------------------------------------------

package:
  stage: package
  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  script:
    - pushd NuGet
    - bash ./check_package_exists.sh
    - nuget pack
    - popd
  artifacts:
    when: always
    expire_in: 4 hours
    paths:
      - "NuGet/*.nupkg"

#-----------------------------------------------------------
#                      DEPLOY
#-----------------------------------------------------------

deploy:
  stage: deploy
  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  script:
    - pushd NuGet
    - nuget source Add -Name gitlab -Source "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/nuget/index.json" -UserName gitlab-ci-token -Password $CI_JOB_TOKEN
    - nuget push *.nupkg -Source gitlab
    - popd
  only:
    - master
  variables:
    GIT_STRATEGY: none
