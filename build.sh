#!/usr/bin/env bash

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "CMAKE_VARS=\"\" $0 <OS> <ARCH> <CONFIG> <TOOL>"
    exit 0
fi

# tolower arguments
G_OS="${1,,}"
G_ARCH="${2,,}"
G_CONFIG="${3,,}"
G_TOOL="${4,,}"

if [[ ! "${G_OS}" == "windows" && ! "${G_OS}" == "centos" && ! "${G_OS}" == "ubuntu" ]]; then
    echo "${G_OS} is not a supported opperating system"
    exit 1
fi
if [[ ! "${G_ARCH}" == "x64" && ! "${G_ARCH}" == "x86" ]]; then
    echo "${G_ARCH} is not a supported architecture"
    exit 1
fi
if [[ ! "${G_CONFIG}" == "debug" && ! "${G_CONFIG}" == "release" ]]; then
    echo "${G_CONFIG} is not a supported configuration"
    exit 1
fi
if [[ ! "${G_TOOL}" == "default" && ! "${G_TOOL}" == "gcc" && ! "${G_TOOL}" == "vc142" ]]; then
    echo "${G_TOOL} is not a supported toolchain"
    exit 1
fi

G_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE}")" && pwd)"
G_SOURCE_DIR="${G_SCRIPT_DIR}/"
G_CMAKE_VARS="-DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF \
            -DBUILD_opencv_apps=OFF -DBUILD_opencv_world=OFF -DWITH_CUDA=OFF -DBUILD_SHARED_LIBS=OFF \
            -DBUILD_opencv_ml=OFF -DBUILD_opencv_objdetect=OFF -DBUILD_opencv_photo=OFF \
            -DBUILD_opencv_shape=OFF -DBUILD_opencv_stitching=OFF -DBUILD_opencv_superres=OFF \
            -DBUILD_opencv_video=OFF -DBUILD_opencv_videostab=OFF -DWITH_FFMPEG=OFF -DWITH_GTK=OFF \
            -DWITH_IPP=OFF -DWITH_WEBP=OFF -DBUILD_WITH_STATIC_CRT=OFF -DBUILD_PNG=ON \
            -Wdev ${CMAKE_VARS}"

G_DEFAULT_BUILD_ROOT="${G_SCRIPT_DIR}/build"
if [[ "${G_OS}" == "windows" ]]; then
    G_BUILD_ARGS="/maxcpucount:`nproc` /consoleloggerparameters:ForceConsoleColor"

    if [[ "${G_TOOL}" == "vc142" || "${G_TOOL}" == "default" ]]; then
        G_TOOL="vc142"
        G_TOOLSET="-T v142"
        G_CMAKE_GENERATOR="Visual Studio 16 2019"
    else
        echo "${G_TOOL} is not supported on ${G_OS}"
        exit 1
    fi

    if [[ "${G_ARCH}" == "x64" ]]; then
        G_CMAKE_ARCH="-A ${G_ARCH}"
    elif [[ "${G_ARCH}" == "x86" ]]; then
        G_CMAKE_ARCH="-A Win32"
    fi

    G_BUILD_DIR="./${G_OS}/${G_TOOL}/${G_ARCH}/"
elif [[ "${G_OS}" == "centos" || "${G_OS}" == "ubuntu" ]]; then
    if [[ "${G_TOOL}" == "gcc" || "${G_TOOL}" == "default" ]]; then
        G_TOOL="gcc"
    else
        echo "${G_TOOL} is not supported on ${G_OS}"
        exit 1
    fi
    if [[ ! "${G_ARCH}" == "x64" ]]; then
        echo "${G_ARCH} not supported on ${G_OS}"
        exit 1
    fi

    G_CMAKE_GENERATOR="Unix Makefiles"
    G_BUILD_ARGS="-j`nproc` VERBOSE=1"
    G_CMAKE_VARS="${G_CMAKE_VARS} -DCMAKE_BUILD_TYPE=${G_CONFIG}"
    G_BUILD_DIR="./${G_OS}/${G_TOOL}/${G_ARCH}/${G_CONFIG}/"
fi

if [[ "${G_CONFIG}" == "debug" ]]; then
    G_CMAKE_VARS="${G_CMAKE_VARS} -DDEBUG_SUFFIX='-debug'"
fi

mkdir -p "${G_BUILD_DIR}"
pushd "${G_BUILD_DIR}"
    cmake -G "${G_CMAKE_GENERATOR}" ${G_CMAKE_ARCH} ${G_TOOLSET} ${G_CMAKE_VARS} "${G_SOURCE_DIR}"
    cmake --build . --config "${G_CONFIG}" -- ${G_BUILD_ARGS}
popd
